var notes = null;
var currentNote = null;
var alreadyExists = null;
var scroll = null;

function loaded() {
	document.addEventListener('touchmove', function (e) { e.preventDefault(); }, false);
	$("#cherryImg").css("left", window.innerWidth / 2 - 107 + "px");
	$("#noteInput").css("height", noteHeight() - 124);

	if (window.innerWidth <= 1000) {
		setUpHidden("#Note");
		$("#saveNote").remove();
	} else {
		$("#notesList").prepend("<li class='listSpacer'> </li>");
	}

	loadNotes();
	notes = $("#notesList > li").length + 1;
	scroll = new iScroll('Home', { hideScrollbar: true, fadeScrollbar: true });
}

function loadNotes() {
	var arr = $.jStorage.index();
	$.each(arr, function(key, value) {
		$("#notesList").prepend("<li class='" + value + "'><a href='#" + value + "' onClick='showNote(\"" + value + "\")'>" + showNoteTitle(value) + "</a></li>");
	});
}

function genNote(i) {
	var appName = "CherryNotes";
	return appName + i;
}

function addNote() {
	showNote(genNote(notes++), true);
}

function showNote(id, creating) {	
	if (creating === undefined) {
		creating = false;
		alreadyExists = true;
	}

	currentNote = id;
	if (creating == false) {
		var json = $.parseJSON($.jStorage.get(id));
		$("#noteInput").val(json.note);
	} else {
		$("#noteInput").val("");
	}
	
	// Setup the buttons
	setupMailTo();

	if (window.innerWidth >= 1000) {
		$("#Note").css("width", window.innerWidth - 300 + "px");
		fadeInFromOpaque("#Note");
		$("#Note").css("padding-left", ((window.innerWidth - $("#noteInput").outerWidth() - 300) / 2) - 8 + "px");
		fadeIn("#saveNote");
		fadeOut("#addNote");
	} else {
		$("#Note").removeClass("hidden");
		fadeIn("#backButton");
		fadeOut("#addNote");
		
		if (window.innerWidth <= 480) {
			fadeOut("#cherryImg");
		}
		
		$("#Home").animate({ "left": "-=" + window.innerWidth }, "slow", function() {
			$("#Home").addClass("hidden");
		});
		$("#Note").animate({ "left": "-=" + window.innerWidth }, "slow", function() {
			$("#noteInput").css("left", window.innerWidth / 2 - $("#noteInput").outerWidth(true) / 2 + "px");
		});
	}
}

function hideNote() {
	var noteTitle = $("#noteInput").val().split("\n");
	var json = { "title": noteTitle[0], "note": $("#noteInput").val() };
	var jsString = JSON.stringify(json);

	if ($("#noteInput").val() != "") {
		if (alreadyExists === true) {
			// Add function to change the title of the li if needed
			var currID = "." + currentNote;

			$.jStorage.deleteKey(currentNote);
			$(currID).remove();
			$.jStorage.set(currentNote, jsString);
			$("#notesList").prepend("<li class='" + currentNote + "'><a href='#" + currentNote + "' onClick='showNote(\"" + currentNote + "\")'>" + noteTitle[0] + "</a></li>");
		} else {
			$.jStorage.set(currentNote, jsString);
			$("#notesList").prepend("<li class='" + currentNote + "'><a href='#" + currentNote + "' onClick='showNote(\"" + currentNote + "\")'>" + noteTitle[0] + "</a></li>");
		}
	} else {
		// Do nothing
	}

	if (window.innerWidth <= 480) {
		$("#Home").removeClass("hidden");
		fadeOut("#backButton");
		fadeIn("#cherryImg");
	} else {
		fadeOut("#saveNote");
	}


	scroll.refresh();
	fadeIn("#addNote");
	
	if (window.innerWidth <= 849) {
		$("#Home").animate({ "left": "+=" + window.innerWidth }, "slow");
		$("#Note").animate({ "left": "+=" + window.innerWidth }, "slow", function() {
			$("#Note").addClass("hidden");
			scroll.refresh();
		});
	}
}

function fadeOut(element) {
	$(element).animate({ "opacity": "0" }, "slow", function() {
		$(element).addClass("hidden");
	});
}

function fadeIn(element) {
	$(element).removeClass("hidden");
	$(element).animate({ "opacity": "1" }, "slow");
}

function fadeInFromOpaque(element) {
	$(element).css("opacity", "0");
	$(element).removeClass("hidden");
	$(element).animate({ "opacity": "1" }, "slow");
}

function showNoteTitle(id) {
	var json = $.parseJSON($.jStorage.get(id));
	return json.title;
}

function setUpHidden(element) {
	var startPos = $(window).width();
	$(element).css("left", startPos);
}

function noteHeight() {
	return Math.floor($("#Note").innerHeight() / 20) * 20;
}

function randomString() {
	var chars = "abcdefghiklmnopqrstuvwxyz";
	var rlength = 5;
	var random = "";

	for (var i = 0; i < rlength; i++) {
		var rnum = Math.floor(Math.random() * chars.length);
		random += chars.substring(rnum, rnum + 1);
	}

	lastRandom = random;
	return random;
}

function trashNote() {
	var currID = "." + currentNote;

	$.jStorage.deleteKey(currentNote);
	$(currID).remove();
	$("#Home").removeClass("hidden");
	fadeOut("#backButton");
	
	if (window.innerWidth <= 480) {
		fadeIn("#cherryImg");
	} else {
		// Do nothing
	}

	fadeIn("#addNote");
	$("#Home").animate({ "left": "+=" + window.innerWidth }, "slow");
	$("#Note").animate({ "left": "+=" + window.innerWidth }, "slow", function() {
		$("#Note").addClass("hidden");
	});
}

function setupMailTo() {
	var noteTitle = $("#noteInput").val().split("\n");
	var note = $("#noteInput").val();
	var href = "mailto:?subject=CherryNotes: " + noteTitle[0] + "&body=" + note.replace(/\n/g, "%0A"); 
	$("#linkMail").attr("href", href);
}

function onNoteBlur() {
	setupMailTo();
	window.scrollTo(0, 0);
	if (window.innerWidth >= 1000) {
		fadeIn("#saveNote");
	}
}